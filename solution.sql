USE music_db;
-- Artists that has letter D in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- All songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length FROM artists 
    JOIN albums ON artists.id = albums.artist_id;
    JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT artists.name, albums.album_title FROM artists 
    JOIN albums ON artists.id = albums.artist_id;
    WHERE album_title LIKE "%a%";

-- Sort the albums in Z-A order.
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A and songs from A-Z)
SELECT albums.album_title, songs.song_name FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id
    ORDER BY song_name ASC, album_title DESC;